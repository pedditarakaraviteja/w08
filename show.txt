So how can you figure out which types of food-related content will resonate in a very crowded space?
 One strategy is to focus on food and restaurant trends, especially among the biggest digital content consumers: 
the millennials. CBD Marketing�s analysis of 12.5 million social media posts by U.S.
 millennials (ages 18 to 35) revealed that they: